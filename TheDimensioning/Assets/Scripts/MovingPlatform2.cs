﻿using UnityEngine;
using System.Collections;

public class MovingPlatform2 : MonoBehaviour {


    private Transform currentPosition;

    public GameObject platform;
    public float moveSpeed;
    public Transform[] points;
    public int pointSelection = 1;

	//Sets first position to approach
	void Awake () {
        currentPosition = points[pointSelection];
        
	}
	
	// Update is called once per frame
	void Update () {
        //moves platform towards destination
        platform.transform.position = Vector3.MoveTowards(platform.transform.position, currentPosition.position, Time.deltaTime * moveSpeed);

        //if platfrom reaches destiantion, cycle to the next point to move towards. 
        if(platform.transform.position == currentPosition.position)
        {
            pointSelection++;

            if (pointSelection == points.Length)
                pointSelection = 0;

            currentPosition = points[pointSelection];
        }

    }
}
