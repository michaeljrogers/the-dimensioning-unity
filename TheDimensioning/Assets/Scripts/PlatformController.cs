﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlatformController : MonoBehaviour
{

    [HideInInspector]
    public bool facingRight = true;
    [HideInInspector]
    public bool jump = false;
    Scene scene;
    
    //Movement Variables
    public float moveForce = 365f;
    public float maxSpeed = 8f;
    public float jumpForce = 1000f;


    //Ground Check Variables
    public Transform groundCheck;
    private bool grounded = true;
    float groundRadius = .9f;
    public LayerMask whatIsGround;

    private Animator anim;
    private Rigidbody2D rb2d;

    public LevelManager levelManager;
    public GameObject startPoint;


    // Use this for initialization
    void Awake()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }
   

    // Update is called once per frame
    void Update()
    {

        //Determines if character is on the ground
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
       
        //If 'R' key is pressed, set spawn to default and restart level
        if (Input.GetKeyDown(KeyCode.R))
        {
            PlayerPrefs.SetFloat("respawnX", startPoint.transform.position.x);
            PlayerPrefs.SetFloat("respawnY", startPoint.transform.position.y);
            PlayerPrefs.SetFloat("respawnZ", startPoint.transform.position.z);
            levelManager.Respawn();

        }

        //If 'Space' key is pressed, set jump to true.
        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            jump = true;
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        anim.SetFloat("Speed", Mathf.Abs(h));

        if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();

        if (jump)
        {
            anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }
    }

    //flips character sprite around
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }


    //sets parent to moving platform in order to enable gravity for platform riding.
    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.transform.tag == "MovingPlatform")
        {
            transform.parent = other.transform;
        }


    }

    //Turns off moving platform gravity 
    void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.tag == "MovingPlatform")
        {
            transform.parent = null;
        }
        
    }


}
