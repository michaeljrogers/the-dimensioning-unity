﻿using UnityEngine;
using System.Collections;

public class PlatformFall : MonoBehaviour {

    public float fallDelay =1f;
    
    private Rigidbody2D rb2d;

	void Awake () {
        rb2d = GetComponent<Rigidbody2D>();
	}

    //Causes Platform to fall on collision
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
            Invoke("Fall", fallDelay);
    }

    //Sets platform kinematic to false, causing gravity to take effect
    void Fall()
    {
        rb2d.isKinematic = false;
    }

}
