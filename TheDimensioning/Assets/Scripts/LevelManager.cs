﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    private PlatformController player;
    private Scene scene;
    private Vector3 spawnLocation;

    // Use this for initialization


    //Sets spawn location then moves player to location
    void Start () {
        player = FindObjectOfType<PlatformController>();
        spawnLocation = new Vector3(PlayerPrefs.GetFloat("respawnX"), PlayerPrefs.GetFloat("respawnY"), PlayerPrefs.GetFloat("respawnZ"));
        player.transform.position = spawnLocation;
    }
	
    //Restarts game
    public void Respawn()
    {
        scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
   

    }

    //Sets spawn point by setting coordinates to PlayerPrefs
    public void SetSpawnPoint(GameObject point)
    {
        PlayerPrefs.SetFloat("respawnX", point.transform.position.x);
        PlayerPrefs.SetFloat("respawnY", point.transform.position.y);
        PlayerPrefs.SetFloat("respawnZ", point.transform.position.z);
    }
}
