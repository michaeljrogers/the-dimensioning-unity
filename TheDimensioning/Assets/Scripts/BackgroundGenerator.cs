﻿using UnityEngine;
using System.Collections;

public class BackgroundGenerator : MonoBehaviour {

    public GameObject background;
    private Vector3 originPosition;
    public int backgroundCount;
    public float backgroundSize =20f;

	//Determines starting position for background to begin rendering.
	void Start () {
        originPosition = transform.position;
        Generate();

	}
	
	// Generates n number of background wallpapers
	void Generate () {
	    for (int i = 0; i < backgroundCount; i++)
        {
            Vector3 nextPosition = originPosition + new Vector3(backgroundSize, 0, 0);
            Instantiate(background, nextPosition, Quaternion.identity);
            originPosition = nextPosition;
        }
	}
}
