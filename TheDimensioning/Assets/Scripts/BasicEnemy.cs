﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class BasicEnemy : MonoBehaviour {

    private Transform currentPosition;
    private Scene scene;

    public GameObject enemy;
    public float moveSpeed;
    public Transform[] points;
    public int pointSelection = 1;

    // Sets enemy first position to move towards
    void Awake()
    {
        currentPosition = points[pointSelection];

    }

    // Update is called once per frame
    void Update()
    {
        //moves enemy towards position
        enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, currentPosition.position, Time.deltaTime * moveSpeed);

        //if position is reached, cycle to new position on patrol
        if (enemy.transform.position == currentPosition.position)
        {
            pointSelection++;

            if (pointSelection == points.Length)
                pointSelection = 0;

            currentPosition = points[pointSelection];
        }

    }

    //Kills player on collision
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

    }
}