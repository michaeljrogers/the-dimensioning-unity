﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour
{
    public LevelManager levelManager;

    //Finds our level manager
    void Start ()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }
    
    //When player collides with checkpoint, pass checkpoint spawn location to levelManager and hide checkpoint object
    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            levelManager.SetSpawnPoint(gameObject);
            gameObject.SetActive(false);

            
        }
    }
}
