﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DeathTrigger : MonoBehaviour {

    public LevelManager levelManager;

    //Locates our levelManager object
    void Start ()
    {
        levelManager = FindObjectOfType<LevelManager>();

    }

    //On collision with DeathTrigger, invoke levelManager.Respawn()
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            levelManager.Respawn();
        }

    }

}
